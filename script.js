//Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//Когда происходит событие, ему можно назначить обработчик, то есть функцию, которая сработает.

const input = document.getElementsByClassName('input')[0];
const priceBox = document.getElementsByClassName('priceBox')[0];

input.addEventListener('blur', function( event ) {
  const inputValue = event.target.value;
  const spanElement = document.createElement('span');
  const closeIconElement = document.createElement('span');
  const errorBlock = document.createElement('div');

  if (inputValue < 0) {
    input.classList.add('error');
    input.after(errorBlock);
    errorBlock.classList.add('errorText');
    errorBlock.textContent = `Please enter correct price.`;
  } else {
    const errorTextBlock = document.getElementsByClassName('errorText')[0];
    input.classList.remove('error');

    if (errorTextBlock) {
      errorTextBlock.remove();
    }

    spanElement.classList.add('price');
    spanElement.textContent = `Текущая цена: ${inputValue}`;
    priceBox.appendChild(spanElement);

    closeIconElement.classList.add('closeIcon');
    spanElement.appendChild(closeIconElement);
    closeIconElement.addEventListener('click', function () {
      spanElement.remove();
      input.value = '';
    })
  }
});

